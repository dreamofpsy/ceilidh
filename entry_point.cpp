#include "audio.h"
#include "game_board.h"
#include "game_play.h"
#include "render.h"
#include "sdl_operators.h"
#include <SDL.h>
#include <stdio.h>
#include <algorithm>

// -------------------------------------------------------------------------------------------------------------

int SDL_main(int argc, char ** argv)
{
	SDL_Init(SDL_INIT_EVERYTHING);

	const SDL_Point window_dimensions = R_WindowDimensions();
	SDL_Window* const window = SDL_CreateWindow(
		"Ceilidh",        
		SDL_WINDOWPOS_UNDEFINED, 
		SDL_WINDOWPOS_UNDEFINED, 
		window_dimensions.x,                     
		window_dimensions.y,                     
		SDL_WINDOW_OPENGL);

	R_RenderData render_data = R_InitializeRenderer(window);
	R_RenderTitle(render_data);

	SDL_AudioSpec wav_spec;
	A_AudioData piece_move_audio = A_LoadWav("piece_move.wav", wav_spec);
	A_AudioData piece_combine_audio = A_LoadWav("piece_combine.wav", wav_spec);
	A_AudioData piece_combine_match_audio = A_LoadWav("piece_combine_match.wav", wav_spec);
	A_AudioData piece_split_audio = A_LoadWav("piece_split.wav", wav_spec);
	A_AudioData piece_reset_audio = A_LoadWav("piece_reset.wav", wav_spec);
	A_AudioData sequence_3_audio = A_LoadWav("sequence_3.wav", wav_spec);
	A_AudioData sequence_5_audio = A_LoadWav("sequence_5.wav", wav_spec);
	A_AudioData match_complete_audio = A_LoadWav("piece_match_complete.wav", wav_spec);

	A_AudioData current_audio = piece_combine_audio;
	A_Setup(current_audio, wav_spec);

	SDL_Event ev = {};
	while (ev.type != SDL_QUIT && ev.type != SDL_MOUSEBUTTONDOWN)
	{
		SDL_WaitEvent(&ev);
	}

	if (ev.type != SDL_QUIT)
	{
		ev = {};
	}

	G_Board board = G_InitialSetup();
	R_Render(render_data, board);
	current_audio = piece_move_audio;

	SDL_Point selected_square = { -1, -1 };
	for (;ev.type != SDL_QUIT;  ev.type == SDL_QUIT || SDL_WaitEvent(&ev))
	{
		switch (ev.type)
		{
		case SDL_MOUSEBUTTONDOWN:
			selected_square = R_SquareAtScreenPoint(ev.button.x, ev.button.y);
			R_Render(render_data, board, &selected_square);
			break;
		case SDL_MOUSEBUTTONUP:
			if (selected_square == R_SquareAtScreenPoint(ev.button.x, ev.button.y))
			{
			 	G_TurnResult turn_results[PIECE_COUNT];
				G_TurnResult* controlling_result = G_EvaluateTurn(
					turn_results,
					board,
					selected_square.x,
					selected_square.y);

				const ptrdiff_t controlling_piece_index = std::distance(turn_results, controlling_result);
				G_Piece& controlling_piece = board.Pieces[controlling_piece_index];

				if (controlling_result->combined)
				{
					current_audio = controlling_piece.CombinedPieceIndex == controlling_piece.MatchPieceIndex ?
						piece_combine_match_audio :
						piece_combine_audio;
				}
				else if (controlling_result->completed_sequence)
				{
					current_audio = piece_split_audio;
				}
				else if (controlling_result->completed_match_sequence)
				{
					current_audio = match_complete_audio;
				}
				else if (controlling_result->reset_to_start)
				{
					current_audio = piece_reset_audio;
				}
				else
				{
					current_audio = piece_move_audio;
				}

				R_AnimateMovement(ev, render_data, board);

				if (controlling_result->combined)
				{
					SDL_Delay(500);
					
					current_audio = controlling_piece.CombinedPieceIndex == controlling_piece.MatchPieceIndex ?
						sequence_5_audio :
						sequence_3_audio;
					
					R_AnimateSequence(
						ev,
						render_data,
						board,
						controlling_piece.SequencePositions,
						controlling_piece.SequenceCount);
				}

				if (controlling_result->completed_match_sequence)
				{
					SDL_Delay(1000);

					R_RenderTitle(render_data);
					current_audio = piece_combine_audio;
					while (ev.type != SDL_QUIT && ev.type != SDL_MOUSEBUTTONDOWN)
					{
						SDL_WaitEvent(&ev);
					}

					if (ev.type != SDL_QUIT)
					{
						ev = {};
						board.ResetAllPieces();
						board.RandomizeRoles();
						board.Validate();
						current_audio = piece_move_audio;
						R_AnimateMovement(ev, render_data, board);
					}
				}
			}
			else
			{
				R_Render(render_data, board);
			}
			break;
		default:
			break;
		}
	}

	R_ReleaseRenderer(render_data);
	A_Release(match_complete_audio);
	A_Release(sequence_5_audio);
	A_Release(sequence_3_audio);
	A_Release(piece_split_audio);
	A_Release(piece_combine_match_audio);
	A_Release(piece_combine_audio);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
