#pragma once
#include <stdint.h>

// -------------------------------------------------------------------------------------------------------------

struct G_Board;
union SDL_Event;
struct SDL_Point;
struct SDL_Renderer;
struct SDL_Texture;
struct SDL_Window;

// -------------------------------------------------------------------------------------------------------------

struct R_RenderData
{
	SDL_Renderer* Renderer = nullptr;
	SDL_Texture* BackgroundTexture = nullptr;
	SDL_Texture* PieceTexture = nullptr;
	SDL_Texture* TitleTexture = nullptr;
};

// -------------------------------------------------------------------------------------------------------------

void R_AnimateMovement(SDL_Event& ev, R_RenderData& render_data, const G_Board& board);

void R_AnimateSequence(
	SDL_Event& ev,
	R_RenderData& render_data,
	const G_Board& board,
	const SDL_Point* sequence_positions,
	int sequence_count);

void R_Render(R_RenderData& render_data, const G_Board& board);
void R_Render(R_RenderData& render_data, const G_Board& board, const SDL_Point* highlight);
void R_Render(R_RenderData& render_data, const G_Board& board, const SDL_Point* highlight, float t);

R_RenderData R_InitializeRenderer(SDL_Window* const window);
void R_ReleaseRenderer(R_RenderData& render_data);

void R_RenderTitle(R_RenderData& render_data);

SDL_Point R_WindowDimensions();
SDL_Point R_SquareAtScreenPoint(int screen_x, int screen_y);
