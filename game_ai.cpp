#include "game_ai.h"
#include "game_board.h"
#include <assert.h>

// -------------------------------------------------------------------------------------------------------------

constexpr int WANDERER_MIN_DISTANCE_FROM_WALLS = 2;
constexpr int GREGARIOUS_MIN_DISTANCE = 5;
constexpr int GREGARIOUS_MIN_DISTANCE_SQ = GREGARIOUS_MIN_DISTANCE * GREGARIOUS_MIN_DISTANCE;

// -------------------------------------------------------------------------------------------------------------

static void G_MovementSeeker(const G_Board& board, int piece_index, int& new_x, int& new_y);
static void G_MovementWanderer(const G_Board& board, int piece_index, int& new_x, int& new_y);
static void G_MovementGregarious(const G_Board& board, int piece_index, int& new_x, int& new_y);

// -------------------------------------------------------------------------------------------------------------

void G_ComputeDesiredMovement(const G_Board& board, int piece_index, int& new_x, int& new_y)
{
	const G_Piece& piece = board.Pieces[piece_index];
	if (piece.Rank == G_PieceRank::CombinedLeader)
	{
		const SDL_Point next_sequence_point = piece.SequencePositions[piece.SequenceIndex];
		new_x = next_sequence_point.x;
		new_y = next_sequence_point.y;
	}
	else
	{
		switch (piece.Role)
		{
		case G_PieceRole::Seeker:
			G_MovementSeeker(board, piece_index, new_x, new_y);
			break;
		case G_PieceRole::Wanderer:
			G_MovementWanderer(board, piece_index, new_x, new_y);
			break;
		case G_PieceRole::Gregarious:
			G_MovementGregarious(board, piece_index, new_x, new_y);
			break;
		default:
			assert(false && "How did we get here?");
			new_x = piece.X;
			new_y = piece.Y;
			break;
		}
	}
}

// -------------------------------------------------------------------------------------------------------------

static void G_MovementSeeker(const G_Board& board, int piece_index, int& new_x, int& new_y)
{
	const G_Piece& piece = board.Pieces[piece_index];
	assert(piece.MatchPieceIndex != INVALID_PIECE);

	const G_Piece& match_piece = board.Pieces[piece.MatchPieceIndex];
	if (match_piece.CombinedPieceIndex != INVALID_PIECE)
	{
		new_x = match_piece.X;
		new_y = match_piece.Y;
	}
	else
	{
		G_MovementWanderer(board, piece_index, new_x, new_y);
	}
}

// -------------------------------------------------------------------------------------------------------------

static void G_MovementWanderer(const G_Board& board, int piece_index, int& new_x, int& new_y)
{
	const G_Piece& piece = board.Pieces[piece_index];
	
	const bool near_vertical_wall =
		piece.X <= WANDERER_MIN_DISTANCE_FROM_WALLS ||
		piece.X > GRID_WIDTH - WANDERER_MIN_DISTANCE_FROM_WALLS;
	
	SDL_Point next_point;
	if (near_vertical_wall)
	{
		new_x = GRID_WIDTH / 2;
		new_y = piece.Y;
	}
	else if (board.GenerateRandomAvailablePositionsAround(&next_point, 1, piece.X, piece.Y, piece_index))
	{
		new_x = next_point.x;
		new_y = next_point.y;
	}
	else
	{
		new_x = piece.X;
		new_y = piece.Y;
	}
}

// -------------------------------------------------------------------------------------------------------------

static void G_MovementGregarious(const G_Board& board, int piece_index, int& new_x, int& new_y)
{
	const G_Piece& piece = board.Pieces[piece_index];

	int nearest_piece_index = INVALID_PIECE;
	int nearest_piece_distance_sq = GREGARIOUS_MIN_DISTANCE_SQ;
	for (int other_piece_index = 0; other_piece_index < board.PieceCount; ++other_piece_index)
	{
		const G_Piece& other_piece = board.Pieces[other_piece_index];
		if (other_piece_index != piece_index &&
			other_piece_index != piece.LastCombinedPieceIndex &&
			other_piece.CombinedPieceIndex == INVALID_PIECE &&
			G_Piece::CanCombine(piece, other_piece))
		{
			const int dist_x = other_piece.X - piece.X;
			const int dist_y = other_piece.Y - piece.Y;
			const int other_distance_sq = dist_x * dist_x + dist_y * dist_y;
			if (other_distance_sq <= nearest_piece_distance_sq)
			{
				nearest_piece_index = other_piece_index;
				nearest_piece_distance_sq = other_distance_sq;
			}
		}
	}

	if (nearest_piece_index != INVALID_PIECE)
	{
		const G_Piece& nearest_piece = board.Pieces[nearest_piece_index];
		new_x = nearest_piece.X;
		new_y = nearest_piece.Y;
	}
	else
	{
		G_MovementWanderer(board, piece_index, new_x, new_y);
	}
}