#include "audio.h"
#include <algorithm>

// -------------------------------------------------------------------------------------------------------------
static void A_AudioCallback(void* data, uint8_t* stream, int length);

// -------------------------------------------------------------------------------------------------------------

A_AudioData A_LoadWav(const char* filename, SDL_AudioSpec& wav_spec)
{
	A_AudioData audio_data;
	SDL_LoadWAV(
		filename,
		&wav_spec,
		&audio_data.Bytes,
		&audio_data.LengthRemaining);
	
	audio_data.Format = wav_spec.format;
	audio_data.DebugName = filename;
	return audio_data;
}

// -------------------------------------------------------------------------------------------------------------

void A_Release(A_AudioData& data)
{
	SDL_FreeWAV(data.Bytes);
	data.Bytes = nullptr;
	data.LengthRemaining = 0;
}

// -------------------------------------------------------------------------------------------------------------

void A_Setup(A_AudioData& playing_data, SDL_AudioSpec wav_spec)
{
	wav_spec.userdata = &playing_data;
	wav_spec.callback = A_AudioCallback;
	SDL_OpenAudio(&wav_spec, nullptr);
	SDL_PauseAudio(0);
}

// -------------------------------------------------------------------------------------------------------------

static void A_AudioCallback(void* data, uint8_t* stream, int length)
{
    SDL_memset(stream, 0, length);

	auto audio_data = static_cast<A_AudioData*>(data);
	if (audio_data && audio_data->LengthRemaining)
    {
		int bytes_to_mix = std::min<int>(length, audio_data->LengthRemaining);
		SDL_MixAudioFormat(
			stream,
			audio_data->Bytes,
			audio_data->Format,
			bytes_to_mix,
			SDL_MIX_MAXVOLUME / 2);

		audio_data->Bytes += bytes_to_mix;
		audio_data->LengthRemaining -= bytes_to_mix;
	}
}