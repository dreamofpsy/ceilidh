#include "game_board.h"
#include <algorithm>
#include <random>
#include <assert.h>

// -------------------------------------------------------------------------------------------------------------

static const int NUM_SEEKERS = 2;
static const int NUM_WANDERERS = 2;
static const int NUM_GREGARIOUS = PIECE_COUNT - NUM_SEEKERS - NUM_WANDERERS;

// -------------------------------------------------------------------------------------------------------------

bool G_Piece::CanCombine(const G_Piece& piece_a, const G_Piece& piece_b)
{
	return
		piece_a.Rank == G_PieceRank::Singleton &&
		piece_b.Rank == G_PieceRank::Singleton &&
		std::abs(piece_a.ColourIndex - piece_b.ColourIndex) >= 0.5f;	
}

// -------------------------------------------------------------------------------------------------------------

G_Board::G_Board()
{
	std::fill(std::begin(PieceGridMap), std::end(PieceGridMap), INVALID_PIECE);
}

// -------------------------------------------------------------------------------------------------------------

void G_Board::Validate() const
{
#if !defined(NDEBUG)
	for (int y = 0; y < GRID_HEIGHT; ++y)
	{
		for (int x = 0; x < GRID_WIDTH; ++x)
		{
			int8_t piece_index = PieceGridMap[GridIndex(x,y)];
			if (piece_index != INVALID_PIECE)
			{
				const G_Piece& piece = Pieces[piece_index];
				assert(piece.X == x && piece.Y == y && "piece is not where the grid thinks it is");
			}
		}
	}

	for (int piece_index = 0; piece_index < PieceCount; ++piece_index)
	{
		const G_Piece& piece = Pieces[piece_index];
		if (piece.Rank == G_PieceRank::Singleton || piece.Rank == G_PieceRank::CombinedLeader)
		{
			size_t grid_index = GridIndex(piece.X, piece.Y);
			assert(PieceGridMap[grid_index] == piece_index && "piece is not at the correct grid map location");
		}

		switch (piece.Role)
		{
		case G_PieceRole::Seeker:
		case G_PieceRole::Wanderer:
		case G_PieceRole::Gregarious:
			break;
		default:
			assert(false && "How did we get here?");
			break;
		}
	}
#endif
}

// -------------------------------------------------------------------------------------------------------------

void G_Board::RandomizeRoles()
{
    std::random_device rd;
    std::mt19937 rng(rd());
	
	G_PieceRole roles[PIECE_COUNT];
	auto first_gregarious = std::begin(roles);
	auto first_seeker = first_gregarious + NUM_GREGARIOUS;
	auto first_wanderer = first_seeker + NUM_SEEKERS;

	std::fill(first_gregarious, first_seeker, G_PieceRole::Gregarious);
	std::fill(first_seeker, first_wanderer, G_PieceRole::Seeker);
	std::fill(first_wanderer, std::end(roles), G_PieceRole::Wanderer);

	std::shuffle(std::begin(roles), std::end(roles), rng);
	
	for (int piece_index = 1; piece_index < PieceCount; ++piece_index)
	{
		Pieces[piece_index].Role = roles[piece_index];
	}
}

// -------------------------------------------------------------------------------------------------------------

void G_Board::ResetAllPieces()
{
	std::fill(std::begin(PieceGridMap), std::end(PieceGridMap), INVALID_PIECE);
	
	for (int piece_index = 0; piece_index < PieceCount; ++piece_index)
	{
		G_Piece& piece =  Pieces[piece_index];
		piece.SequenceIndex = 0;
		piece.SequenceCount = 0;
		piece.Rank = G_PieceRank::Singleton;
		piece.Role = G_PieceRole::Gregarious;
		piece.CombinedPieceIndex = INVALID_PIECE;
		piece.LastCombinedPieceIndex = INVALID_PIECE;
		piece.PrevX = piece.X;
		piece.PrevY = piece.Y;
		piece.X = piece.StartX;
		piece.Y = piece.StartY;

		PieceGridMap[GridIndex(piece.X, piece.Y)] = piece_index;
	}
}

// -------------------------------------------------------------------------------------------------------------

void G_Board::RemoveFadingPieces()
{
	for (auto& square : PieceGridMap)
	{
		if (square != INVALID_PIECE && Pieces[square].Rank == G_PieceRank::Fading)
		{
			square = INVALID_PIECE;
		}
	}
}

// -------------------------------------------------------------------------------------------------------------

void G_Board::RemoveAllBut(int8_t piece_index)
{
	for (auto& square : PieceGridMap)
	{
		if (square != INVALID_PIECE && square != piece_index)
		{
			Pieces[square].Rank = G_PieceRank::Fading;
		}
	}
}

// -------------------------------------------------------------------------------------------------------------

void G_Board::RemovePiece(int8_t piece_index)
{
	for (auto& square : PieceGridMap)
	{
		if (square == piece_index)
		{
			square = INVALID_PIECE;
			break;
		}
	}
}

// -------------------------------------------------------------------------------------------------------------

int8_t G_Board::PieceAt(int x, int y) const
{
	return PieceGridMap[GridIndex(x, y)];
}

// -------------------------------------------------------------------------------------------------------------

int8_t G_Board::NewPiece(float colour_index)
{
	G_Piece& piece = Pieces[PieceCount];
	piece.ColourIndex = colour_index;
	piece.Rank = G_PieceRank::Singleton;
	piece.CombinedPieceIndex = INVALID_PIECE;
	piece.LastCombinedPieceIndex = INVALID_PIECE;

	return PieceCount++;
}

// -------------------------------------------------------------------------------------------------------------

void G_Board::ResetPiece(uint8_t piece_index)
{
	G_Piece& piece = Pieces[piece_index];
	piece.SequenceIndex = 0;
	piece.SequenceCount = 0;
	piece.Rank = G_PieceRank::Singleton;
	piece.LastCombinedPieceIndex = INVALID_PIECE;
	
	SDL_Point nearest_to_start = NearestOpenPieceTo(piece.StartX, piece.StartY, piece_index);
	if (piece.CombinedPieceIndex != INVALID_PIECE)
	{
		SplitPieces(piece_index, nearest_to_start.x, nearest_to_start.y);
	}
	else 
	{
		MovePiece(piece_index, nearest_to_start.x, nearest_to_start.y);
	}
	
	assert(PieceGridMap[GridIndex(nearest_to_start.x, nearest_to_start.y)] == piece_index);
}

// -------------------------------------------------------------------------------------------------------------

void G_Board::PutPiece(int8_t piece_index, int x, int y)
{
	G_Piece& piece = Pieces[piece_index];
	piece.X = x;
	piece.Y = y;
	piece.PrevX = x;
	piece.PrevY = y;
	piece.StartX = x;
	piece.StartY = y;
	
	PieceGridMap[GridIndex(x, y)] = piece_index;
}

// -------------------------------------------------------------------------------------------------------------

void G_Board::MovePiece(int8_t piece_index, int new_x, int new_y)
{
	G_Piece& piece = Pieces[piece_index];

	int8_t& last_square = PieceGridMap[GridIndex(piece.X, piece.Y)];
	if (last_square == piece_index)
	{
		last_square = INVALID_PIECE;
	}

	assert(PieceGridMap[GridIndex(new_x, new_y)] == INVALID_PIECE);
	PieceGridMap[GridIndex(new_x, new_y)] = piece_index;

	piece.PrevX = piece.X;
	piece.PrevY = piece.Y;
	piece.X = new_x;
	piece.Y = new_y;
}

// -------------------------------------------------------------------------------------------------------------

void G_Board::CombinePieces(int8_t piece_a_index, int8_t piece_b_index, int new_x, int new_y)
{
	G_Piece& piece_a = Pieces[piece_a_index];
	G_Piece& piece_b = Pieces[piece_b_index];

	piece_a.Rank = G_PieceRank::CombinedLeader;
	piece_a.CombinedPieceIndex = piece_b_index;

	PieceGridMap[GridIndex(piece_b.X, piece_b.Y)] = INVALID_PIECE;
	MovePiece(piece_a_index, new_x, new_y);
	
	piece_b.CombinedPieceIndex = piece_a_index;
	piece_b.Rank = G_PieceRank::CombinedFollower;
	
	piece_a.LastCombinedPieceIndex = piece_b_index;
	piece_b.LastCombinedPieceIndex = piece_a_index;
}

// -------------------------------------------------------------------------------------------------------------

void G_Board::SplitPieces(int8_t piece_a_index, int new_a_x, int new_a_y)
{
	G_Piece& piece_a = Pieces[piece_a_index];
	
	int8_t piece_b_index = piece_a.CombinedPieceIndex;
	int8_t piece_b_x = piece_a.X;
	int8_t piece_b_y = piece_a.Y;

	piece_a.Rank = G_PieceRank::Singleton;
	piece_a.CombinedPieceIndex = INVALID_PIECE;

	MovePiece(piece_a_index, new_a_x, new_a_y);

	G_Piece& piece_b = Pieces[piece_b_index];
	piece_b.Rank = G_PieceRank::Singleton;
	piece_b.CombinedPieceIndex = INVALID_PIECE;
	piece_b.PrevX = piece_b_x;
	piece_b.PrevY = piece_b_y;

	MovePiece(piece_b_index, piece_b_x, piece_b_y);
}

// -------------------------------------------------------------------------------------------------------------

int G_Board::GenerateRandomAvailablePositionsAround(
	SDL_Point* output_points,
	int requested_count,
	int center_x,
	int center_y,
	int8_t ignore_piece_index) const
{
	SDL_Point available_points[] = {
		{ center_x - 1, center_y - 1},
		{ center_x, center_y - 1},
		{ center_x + 1, center_y - 1},
		{ center_x - 1, center_y},
		{ center_x, center_y},
		{ center_x + 1, center_y},
		{ center_x - 1, center_y + 1},
		{ center_x , center_y + 1},
		{ center_x + 1, center_y + 1},
	};

	auto first_unavailable = std::remove_if(
		std::begin(available_points),
		std::end(available_points),
		[this, ignore_piece_index](const SDL_Point& p)
		{
			return 
				p.x < 0 || p.y <= 0 || p.x >= GRID_WIDTH || p.y >= GRID_HEIGHT ||
				(PieceAt(p.x, p.y) != INVALID_PIECE && PieceAt(p.x, p.y) != ignore_piece_index);
		});
	
    std::random_device rd;
    std::mt19937 rng(rd());
	std::shuffle(std::begin(available_points), first_unavailable, rng);
	
	if (const ptrdiff_t available_count = std::distance(std::begin(available_points), first_unavailable))
	{
		for (int i = 0; i < requested_count; ++i)
		{
			output_points[i] = available_points[i % available_count];
		}

		return requested_count;
	}

	return 0;
}

// -------------------------------------------------------------------------------------------------------------

SDL_Point G_Board::NearestOpenPieceTo(int x, int y, int8_t ignore_piece_index) const
{
	if (PieceAt(x, y) == INVALID_PIECE || PieceAt(x, y) == ignore_piece_index)
	{
		return { x, y };
	}

	int best_distance_sq = INT_MAX;
	int best_x = -1;
	int best_y = -1;

	for (int test_y = 0; test_y < GRID_HEIGHT; ++test_y)
	{
		for (int test_x = 0; test_x < GRID_WIDTH; ++test_x)
		{
			int test_piece_index = PieceAt(test_x, test_y);
			if (test_piece_index == INVALID_PIECE || test_piece_index == ignore_piece_index)
			{
				const int delta_x = test_x - x;
				const int delta_y = test_y  - y;
				const int distance_sq = delta_x * delta_x + delta_y * delta_y;

				if (distance_sq < best_distance_sq)
				{
					best_distance_sq = distance_sq;
					best_x = test_x;
					best_y = test_y;
				}
			}
		}
	}

	assert(best_x != -1 && best_y != -1);
	return { best_x, best_y };
}

// -------------------------------------------------------------------------------------------------------------

size_t G_Board::GridIndex(int x, int y)
{
	return x + y * GRID_WIDTH;
}