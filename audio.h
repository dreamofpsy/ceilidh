#pragma once
#include <SDL.h>
#include <stdint.h>

// -------------------------------------------------------------------------------------------------------------

struct A_AudioData
{
	uint8_t* Bytes = nullptr;
	uint32_t LengthRemaining = 0;
	SDL_AudioFormat Format = 0;
	const char* DebugName = nullptr;
};

// -------------------------------------------------------------------------------------------------------------
A_AudioData A_LoadWav(const char* filename, SDL_AudioSpec& wav_spec);
void A_Release(A_AudioData& data);
void A_Setup(A_AudioData& playing_data, SDL_AudioSpec wav_spec);
