#pragma once

// -------------------------------------------------------------------------------------------------------------

struct SDL_Point;

// -------------------------------------------------------------------------------------------------------------

bool operator==(const SDL_Point& lhs, const SDL_Point& rhs);
bool operator!=(const SDL_Point& lhs, const SDL_Point& rhs);