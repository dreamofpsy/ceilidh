@echo off
mkdir sln
pushd sln
cmake -A x64 ../.
popd sln
cmake --build sln --config Release
mkdir release
copy *.bmp release
copy *.wav release
copy bin\*.exe release
copy bin\*.dll release
