#include "sdl_operators.h"
#include <SDL_rect.h>

// -------------------------------------------------------------------------------------------------------------

bool operator==(const SDL_Point& lhs, const SDL_Point& rhs)
{
	return lhs.x == rhs.x && lhs.y == rhs.y;
}

// -------------------------------------------------------------------------------------------------------------

bool operator!=(const SDL_Point& lhs, const SDL_Point& rhs)
{
	return lhs.x != rhs.x || lhs.y != rhs.y;
}
