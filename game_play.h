#pragma once

// -------------------------------------------------------------------------------------------------------------

struct G_Board;

// -------------------------------------------------------------------------------------------------------------

struct G_TurnResult
{
	bool combined = false;
	bool completed_sequence = false;
	bool completed_match_sequence = false;
	bool reset_to_start = false;
};

// -------------------------------------------------------------------------------------------------------------

G_Board G_InitialSetup();
G_TurnResult* G_EvaluateTurn(G_TurnResult* turn_results, G_Board& board, int selected_x, int selected_y);