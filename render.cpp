#include "render.h"
#include "game_board.h"
#include <SDL.h>
#include "assert.h"

// -------------------------------------------------------------------------------------------------------------

static const SDL_Color MOUSE_DOWN_COLOUR = { 0xFF, 0xFF, 0xFF, SDL_ALPHA_OPAQUE / 2 };
static const SDL_Color PIECE_LEFT_COLOUR = { 0xFF, 0xB3, 0x47, SDL_ALPHA_OPAQUE};
static const SDL_Color PIECE_RIGHT_COLOUR = { 0x77, 0xC3, 0xEC, SDL_ALPHA_OPAQUE };
static const SDL_Color PIECE_COMBINED_COLOUR = { 0xDD, 0xDD, 0xDD, SDL_ALPHA_OPAQUE };
static const SDL_Color PIECE_MATCHED_COLOUR = { 0xFF, 0xC0, 0xCB, SDL_ALPHA_OPAQUE };
static const SDL_Color TRANSPARENT_COLOUR = {  0xFF, 0xC0, 0xCB,  SDL_ALPHA_TRANSPARENT };
static const SDL_Color SHADOW_COLOUR = {  0x00, 0x00, 0x00,  SDL_ALPHA_OPAQUE };

static const int GRID_SQUARE_DIMENSION = 96;
static const int GRID_BORDER_MARGIN = 16;

static const SDL_Rect GRID_BACKGROUND_RECT = { 
	GRID_BORDER_MARGIN,
	GRID_BORDER_MARGIN,
	GRID_WIDTH * GRID_SQUARE_DIMENSION,
	GRID_HEIGHT * GRID_SQUARE_DIMENSION,
};

static const int WINDOW_WIDTH = GRID_WIDTH * GRID_SQUARE_DIMENSION + 2 * GRID_BORDER_MARGIN;
static const int WINDOW_HEIGHT = GRID_HEIGHT * GRID_SQUARE_DIMENSION + 2 * GRID_BORDER_MARGIN;

static const float PIECE_MOVEMENT_ANIMATION_DURATION = 0.3f;
static const float SEQUENCE_ANIMATION_DURATION_PER_STEP = 0.4f;

static const int COMBINED_PIECE_OFFSET = 8;

// -------------------------------------------------------------------------------------------------------------

enum class R_PieceGraphicType
{
	Shape,
	Outline
};

// -------------------------------------------------------------------------------------------------------------

static float R_Lerp(float start, float end, float t);
static float R_Lerp(int start, int end, float t);
static SDL_Rect R_GenerateSquare(int x, int y);
static SDL_Rect R_GenerateSquare(float x, float y);
static void R_GenerateWhiteSquares(SDL_Rect* rects);
static SDL_Color R_LerpColour(const SDL_Color& start, const SDL_Color& end, float t);
static void R_SetColour(SDL_Renderer* renderer, const SDL_Color& colour);
static void R_SetColour(SDL_Texture* texture, const SDL_Color& colour);
static SDL_Color R_DeterminePieceColour(const G_Board& board, const G_Piece& piece, float t);
static SDL_Texture* R_LoadTexture(SDL_Renderer* renderer, const char* path);
static SDL_Rect R_GeneratePieceSrcRect(int piece_index, R_PieceGraphicType graphic_type);

static void R_BlitPieceAt(
	R_RenderData& render_data,
	const G_Board& board,
	int piece_index,
	SDL_Rect piece_dst_rect,
	int horizontal_offset,
	float t);

static void R_BlitPiece(
	R_RenderData& render_data,
	int piece_index,
	R_PieceGraphicType graphic_type,
	const SDL_Color& colour,
	const SDL_Rect& piece_dst_rect);

// -------------------------------------------------------------------------------------------------------------

void R_AnimateMovement(SDL_Event& ev, R_RenderData& render_data, const G_Board& board)
{
	uint32_t current_time = SDL_GetTicks();
	uint32_t last_time = current_time;

	for (float t = 0.0f; t <= PIECE_MOVEMENT_ANIMATION_DURATION; t += (current_time - last_time) / 1000.0f)
	{
		while (SDL_PollEvent(&ev)) 
		{
			if (ev.type == SDL_QUIT)
			{
				return;
			}
		}

		R_Render(render_data, board, nullptr, t / PIECE_MOVEMENT_ANIMATION_DURATION);
		SDL_Delay(1);

		last_time = current_time;
		current_time = SDL_GetTicks();
	}

	R_Render(render_data, board, nullptr, 1.0f);
}

// -------------------------------------------------------------------------------------------------------------

void R_AnimateSequence(
	SDL_Event& ev,
	R_RenderData& render_data,
	const G_Board& board,
	const SDL_Point* sequence_positions,
	int sequence_count)
{
	uint32_t current_time = SDL_GetTicks();
	uint32_t last_time = current_time;

	const float total_duration = SEQUENCE_ANIMATION_DURATION_PER_STEP * sequence_count;
	for (float t = 0.0f; t <= total_duration; t += (current_time - last_time) / 1000.0f)
	{
		while (SDL_PollEvent(&ev)) 
		{
			if (ev.type == SDL_QUIT)
			{
				return;
			}
		}

		auto sequence_index = static_cast<int>(t * sequence_count / total_duration);
		R_Render(render_data, board, &sequence_positions[sequence_index]);
		SDL_Delay(1);

		last_time = current_time;
		current_time = SDL_GetTicks();
	}

	R_Render(render_data, board, nullptr);
}

// -------------------------------------------------------------------------------------------------------------

void R_Render(R_RenderData& render_data, const G_Board& board)
{
	R_Render(render_data, board, nullptr);
}

void R_Render(R_RenderData& render_data, const G_Board& board, const SDL_Point* highlight)
{
	R_Render(render_data, board, highlight, 1.0f);
}

void R_Render(R_RenderData& render_data, const G_Board& board, const SDL_Point* highlight, float t)
{	
	board.Validate();
	
	SDL_Renderer* renderer = render_data.Renderer;
	SDL_RenderCopy(renderer, render_data.BackgroundTexture, nullptr, nullptr);

	if (highlight && highlight->x < GRID_WIDTH && highlight->y < GRID_HEIGHT)
	{
		const SDL_Rect selected_square = R_GenerateSquare(
			static_cast<float>(highlight->x), 
			static_cast<float>(highlight->y));

		R_SetColour(renderer, MOUSE_DOWN_COLOUR);
		SDL_RenderFillRect(renderer, &selected_square);
	}

	for (int y = 0; y < GRID_HEIGHT; ++y)
	{
		for (int x = 0; x < GRID_WIDTH; ++x)
		{
			const int8_t piece_index = board.PieceGridMap[G_Board::GridIndex(x, y)];
			if (piece_index != INVALID_PIECE)
			{
				const G_Piece& piece = board.Pieces[piece_index];
				const float piece_x = R_Lerp(piece.PrevX, x, t);
				const float piece_y = R_Lerp(piece.PrevY, y, t);
				SDL_Rect piece_dst_rect = R_GenerateSquare(piece_x, piece_y);

				int horizontal_offset = 0;
				if (piece.CombinedPieceIndex != INVALID_PIECE)
				{
					horizontal_offset = COMBINED_PIECE_OFFSET;
					R_BlitPieceAt(render_data, board, piece.CombinedPieceIndex, piece_dst_rect, horizontal_offset, t);
				}

				R_BlitPieceAt(render_data, board, piece_index, piece_dst_rect, -horizontal_offset, t);
			}
		}
	}

	SDL_RenderPresent(renderer);
}

// -------------------------------------------------------------------------------------------------------------

R_RenderData R_InitializeRenderer(SDL_Window* const window)
{
	SDL_Renderer* const renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	assert(renderer);
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
	
	return R_RenderData {
		renderer,
		R_LoadTexture(renderer, "board.bmp"),
		R_LoadTexture(renderer, "piece.bmp"),
		R_LoadTexture(renderer, "title.bmp"),
	};
}

// -------------------------------------------------------------------------------------------------------------

void R_ReleaseRenderer(R_RenderData& render_data)
{
	SDL_DestroyTexture(render_data.TitleTexture);
	SDL_DestroyTexture(render_data.PieceTexture);
	SDL_DestroyTexture(render_data.BackgroundTexture);
	SDL_DestroyRenderer(render_data.Renderer);
}

// -------------------------------------------------------------------------------------------------------------

void R_RenderTitle(R_RenderData& render_data)
{
	SDL_RenderCopy(render_data.Renderer, render_data.TitleTexture, nullptr, nullptr);
	SDL_RenderPresent(render_data.Renderer);
}

// -------------------------------------------------------------------------------------------------------------

SDL_Point R_WindowDimensions()
{
	return { WINDOW_WIDTH, WINDOW_HEIGHT };
}

// -------------------------------------------------------------------------------------------------------------

SDL_Point R_SquareAtScreenPoint(int screen_x, int screen_y)
{
	return SDL_Point {
		(screen_x - GRID_BORDER_MARGIN) / GRID_SQUARE_DIMENSION,
		(screen_y - GRID_BORDER_MARGIN) / GRID_SQUARE_DIMENSION
	};
}

// -------------------------------------------------------------------------------------------------------------

static float R_Lerp(int start, int end, float t)
{
	return R_Lerp(static_cast<float>(start), static_cast<float>(end), t);
}

static float R_Lerp(float start, float end, float t)
{
	return start + t * (end - start);
}

// -------------------------------------------------------------------------------------------------------------

static SDL_Rect R_GenerateSquare(int x, int y)
{
	return SDL_Rect {
		x * GRID_SQUARE_DIMENSION + GRID_BORDER_MARGIN,
		y * GRID_SQUARE_DIMENSION + GRID_BORDER_MARGIN,
		GRID_SQUARE_DIMENSION,
		GRID_SQUARE_DIMENSION
	};
}

static SDL_Rect R_GenerateSquare(float x, float y)
{
	return SDL_Rect {
		static_cast<int>(x * GRID_SQUARE_DIMENSION + GRID_BORDER_MARGIN),
		static_cast<int>(y * GRID_SQUARE_DIMENSION + GRID_BORDER_MARGIN),
		GRID_SQUARE_DIMENSION,
		GRID_SQUARE_DIMENSION
	};
}

// -------------------------------------------------------------------------------------------------------------

static void R_GenerateWhiteSquares(SDL_Rect* rects)
{
	for (int y = 0; y < GRID_HEIGHT; ++y)
	{
		for (int x = 0; x < GRID_WIDTH; ++x)
		{
			const size_t index = G_Board::GridIndex(x, y);
			if (index % 2 == y % 2)
			{
				*rects++ = R_GenerateSquare(x, y);
			}
		}
	}
}

// -------------------------------------------------------------------------------------------------------------

static SDL_Color R_LerpColour(const SDL_Color& start, const SDL_Color& end, float t)
{
	return SDL_Color {
		static_cast<Uint8>(R_Lerp(start.r, end.r, t)),
		static_cast<Uint8>(R_Lerp(start.g, end.g, t)),
		static_cast<Uint8>(R_Lerp(start.b, end.b, t)),
		static_cast<Uint8>(R_Lerp(start.a, end.a, t)),
	};
}

// -------------------------------------------------------------------------------------------------------------

static void R_SetColour(SDL_Renderer* renderer, const SDL_Color& colour)
{
	SDL_SetRenderDrawColor(
		renderer,
		colour.r,
		colour.g,
		colour.b,
		colour.a);
}

// -------------------------------------------------------------------------------------------------------------

static void R_SetColour(SDL_Texture* texture, const SDL_Color& colour)
{
	SDL_SetTextureColorMod(texture, colour.r, colour.g, colour.b);
	SDL_SetTextureAlphaMod(texture, colour.a);
}

// -------------------------------------------------------------------------------------------------------------

static SDL_Color R_DeterminePieceColour(const G_Board& board, const G_Piece& piece, float t)
{
	switch (piece.Rank)
	{
		case G_PieceRank::CombinedLeader:
			return PIECE_COMBINED_COLOUR;
		case G_PieceRank::CombinedFollower:
			return R_DeterminePieceColour(board, board.Pieces[piece.CombinedPieceIndex], t);
		case G_PieceRank::Fading:
			return R_LerpColour(PIECE_MATCHED_COLOUR, TRANSPARENT_COLOUR, t);
		default:
			return R_LerpColour(PIECE_LEFT_COLOUR, PIECE_RIGHT_COLOUR, piece.ColourIndex);
	}
}

// -------------------------------------------------------------------------------------------------------------

static SDL_Texture* R_LoadTexture(SDL_Renderer* renderer, const char* path)
{
	SDL_Surface* bitmap = SDL_LoadBMP(path);
	SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, bitmap);
	SDL_FreeSurface(bitmap);
	
	return texture;
}

// -------------------------------------------------------------------------------------------------------------

static SDL_Rect R_GeneratePieceSrcRect(int piece_index, R_PieceGraphicType graphic_type)
{
	return SDL_Rect{
		GRID_SQUARE_DIMENSION * piece_index,
		GRID_SQUARE_DIMENSION * static_cast<int>(graphic_type),
		GRID_SQUARE_DIMENSION,
		GRID_SQUARE_DIMENSION
	};
}

// -------------------------------------------------------------------------------------------------------------

static void R_BlitPieceAt(
	R_RenderData& render_data,
	const G_Board& board,
	int piece_index,
	SDL_Rect piece_dst_rect,
	int horizontal_offset,
	float t)
{
	const G_Piece& piece = board.Pieces[piece_index];
	piece_dst_rect.x += horizontal_offset;

	if (piece.Rank != G_PieceRank::Fading &&
		(piece.CombinedPieceIndex == INVALID_PIECE ||
		board.Pieces[piece.CombinedPieceIndex].Rank != G_PieceRank::Fading))
	{
		SDL_Color outline_colour = SHADOW_COLOUR;
		if (piece_index == PLAYER_PIECE_INDEX ||
			piece_index == board.Pieces[PLAYER_PIECE_INDEX].MatchPieceIndex)
		{
			outline_colour = R_LerpColour(PIECE_RIGHT_COLOUR, PIECE_LEFT_COLOUR, piece.ColourIndex);
		}
		
		R_BlitPiece(
			render_data,
			piece_index,
			R_PieceGraphicType::Outline,
			outline_colour,
			piece_dst_rect);
	}
	
	R_BlitPiece(
		render_data,
		piece_index,
		R_PieceGraphicType::Shape,
		R_DeterminePieceColour(board, piece, t),
		piece_dst_rect);
}

// -------------------------------------------------------------------------------------------------------------

static void R_BlitPiece(
	R_RenderData& render_data,
	int piece_index,
	R_PieceGraphicType graphic_type,
	const SDL_Color& colour,
	const SDL_Rect& piece_dst_rect)
{
	const SDL_Rect piece_src_rect = R_GeneratePieceSrcRect(piece_index, graphic_type);
	R_SetColour(render_data.PieceTexture, colour);
	SDL_RenderCopy(render_data.Renderer, render_data.PieceTexture, &piece_src_rect, &piece_dst_rect);
}