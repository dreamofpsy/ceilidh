#include "game_play.h"
#include "game_ai.h"
#include "game_board.h"
#include "sdl_operators.h"
#include <algorithm>
#include <numeric>
#include <random>

// -------------------------------------------------------------------------------------------------------------

const int PIECES_TO_SPAWN = PIECE_COUNT;
const float MAX_COLOUR_DIFFERENCE = 0.0f;

// -------------------------------------------------------------------------------------------------------------

static G_TurnResult G_EvaluateTurn(G_Board& board, int piece_index, int selected_x, int selected_y);
static void G_ComputeClampedMovement(const G_Piece& piece, int& selected_square_x, int& selected_square_y);
static void G_GenerateSequence(G_Board& board, int piece_index);

// -------------------------------------------------------------------------------------------------------------

G_Board G_InitialSetup()
{
    std::random_device rd;
    std::mt19937 rng(rd());
	std::uniform_real_distribution<float> colour_dist { 0.0f, MAX_COLOUR_DIFFERENCE };

	G_Board board;
	for (int i = 0; i < PIECES_TO_SPAWN; i += 2)
	{
		board.PutPiece(board.NewPiece(0.0f + colour_dist(rng)), 0, i);
		board.PutPiece(board.NewPiece(1.0f - colour_dist(rng)), GRID_WIDTH - 1, i + 1);
	}
	
	for (int i = 0; i < board.PieceCount; i += 2)
	{
		G_Piece& piece = board.Pieces[i];
		G_Piece& match_piece = board.Pieces[board.PieceCount - i - 1];

		piece.MatchPieceIndex = board.PieceCount - i - 1;
		match_piece.MatchPieceIndex = i;

		if (std::abs(piece.ColourIndex - match_piece.ColourIndex) > 0.5f)
		{
			match_piece.ColourIndex = 1.0f - piece.ColourIndex;
		}
	}

	board.Pieces[PLAYER_PIECE_INDEX].ColourIndex = 0.0f;
	board.Pieces[board.Pieces[PLAYER_PIECE_INDEX].MatchPieceIndex].ColourIndex = 1.0f;

	board.RandomizeRoles();
	board.Validate();
	return board;
}

// -------------------------------------------------------------------------------------------------------------

G_TurnResult* G_EvaluateTurn(G_TurnResult* turn_results, G_Board& board, int selected_x, int selected_y)
{
	board.RemoveFadingPieces();
	board.Validate();

	turn_results[PLAYER_PIECE_INDEX] = G_EvaluateTurn(
		board,
		PLAYER_PIECE_INDEX,
		selected_x,
		selected_y);

	for (int ai_piece_index = 1;
		ai_piece_index < board.PieceCount;
		++ai_piece_index)
	{
		int ai_new_x;
		int ai_new_y;
		G_ComputeDesiredMovement(board, ai_piece_index, ai_new_x, ai_new_y);

		turn_results[ai_piece_index] = G_EvaluateTurn(
			board,
			ai_piece_index,
			ai_new_x,
			ai_new_y);
	}

	G_Piece& player_piece = board.Pieces[PLAYER_PIECE_INDEX];
	if (player_piece.Rank == G_PieceRank::CombinedFollower)
	{
		G_Piece& match_piece = board.Pieces[player_piece.CombinedPieceIndex];
		std::swap(player_piece.Rank, match_piece.Rank);
		player_piece.SequenceCount = match_piece.SequenceCount;
		player_piece.SequenceIndex = match_piece.SequenceIndex;

		board.RemovePiece(player_piece.CombinedPieceIndex);
		board.MovePiece(PLAYER_PIECE_INDEX, match_piece.X, match_piece.Y);
		
		std::copy(
			std::begin(match_piece.SequencePositions),
			std::end(match_piece.SequencePositions),
			player_piece.SequencePositions);

		turn_results[PLAYER_PIECE_INDEX] = turn_results[player_piece.CombinedPieceIndex];
		board.Validate();
	}

	if (player_piece.MatchPieceIndex == player_piece.CombinedPieceIndex)
	{
		board.RemoveAllBut(PLAYER_PIECE_INDEX);
		board.Validate();
	}
	
	return &turn_results[PLAYER_PIECE_INDEX];
}

// -------------------------------------------------------------------------------------------------------------

static G_TurnResult G_EvaluateTurn(G_Board& board, int piece_index, int selected_x, int selected_y)
{
	G_Piece& piece = board.Pieces[piece_index];
	if (piece.Rank == G_PieceRank::CombinedFollower || piece.Rank == G_PieceRank::Fading)
	{
		return { };
	}

	G_TurnResult turn_result;
		
	int new_x = selected_x;
	int new_y = selected_y;
	if (piece.Rank == G_PieceRank::Singleton)
	{
		G_ComputeClampedMovement(piece, new_x, new_y);
	}

	int8_t other_piece_index = board.PieceAt(new_x, new_y);
	if (other_piece_index == INVALID_PIECE || other_piece_index == piece_index)
	{
		if (piece.SequenceCount > 0 &&
			piece.SequenceIndex < piece.SequenceCount &&
			piece.SequencePositions[piece.SequenceIndex] != SDL_Point{new_x, new_y})
		{
			turn_result.reset_to_start = true;
			board.ResetPiece(piece_index);
			board.Validate();
		}
		else if (piece.SequenceCount && ++piece.SequenceIndex >= piece.SequenceCount)
		{
			piece.SequenceIndex = 0;
			piece.SequenceCount = 0;
			if (piece.MatchPieceIndex == piece.CombinedPieceIndex)
			{
				turn_result.completed_match_sequence = true;
				piece.Rank = G_PieceRank::Fading;
				piece.PrevX = piece.X;
				piece.PrevY = piece.Y;
			}
			else
			{
				turn_result.completed_sequence = true;
				board.SplitPieces(piece_index, new_x, new_y);
				board.Validate();
			}
		}
		else
		{
			board.MovePiece(piece_index, new_x, new_y);
			board.Validate();
		}
	}
	else if (G_Piece::CanCombine(piece, board.Pieces[other_piece_index]))
	{
		turn_result.combined = true;
		board.CombinePieces(piece_index, other_piece_index, new_x, new_y);
		board.Validate();
		G_GenerateSequence(board, piece_index);
	}
	else
	{
		turn_result.reset_to_start = true;
		board.ResetPiece(piece_index);
		board.Validate();
	}

	return turn_result;
}

// -------------------------------------------------------------------------------------------------------------

static void G_ComputeClampedMovement(const G_Piece& piece, int& selected_square_x, int& selected_square_y)
{
	const int delta_x = selected_square_x - piece.X;
	if (delta_x != 0)
	{
		selected_square_x = piece.X + delta_x / std::abs(delta_x);
	}
	
	const int delta_y = selected_square_y - piece.Y;
	if (delta_y != 0)
	{
		selected_square_y = piece.Y + delta_y / std::abs(delta_y);
	}
}

// -------------------------------------------------------------------------------------------------------------

static void G_GenerateSequence(G_Board& board, int piece_index)
{
	G_Piece& piece = board.Pieces[piece_index];
	const int center_x = piece.X;
	const int center_y = piece.Y;

	const int sequence_length = piece.CombinedPieceIndex == piece.MatchPieceIndex ?
		SEQUENCE_COUNT_MATCH :
		SEQUENCE_COUNT_FRIEND;

	piece.SequenceCount = board.GenerateRandomAvailablePositionsAround(
		piece.SequencePositions,
		sequence_length,
		center_x,
		center_y,
		piece_index);
}
