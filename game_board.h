#pragma once
#include <SDL_rect.h>
#include <stdint.h>
#include <algorithm>

// -------------------------------------------------------------------------------------------------------------

constexpr int PIECE_COUNT = 10;
constexpr int GRID_WIDTH = PIECE_COUNT;
constexpr int GRID_HEIGHT = PIECE_COUNT;
constexpr int SEQUENCE_COUNT_FRIEND = 3;
constexpr int SEQUENCE_COUNT_MATCH = 5;
constexpr int SEQUENCE_COUNT_MAX = std::max(SEQUENCE_COUNT_FRIEND, SEQUENCE_COUNT_MATCH);
constexpr int PLAYER_PIECE_INDEX = 0;
constexpr int8_t INVALID_PIECE = -1;

// -------------------------------------------------------------------------------------------------------------

enum class G_PieceRank : uint8_t
{
	Singleton,
	CombinedLeader,
	CombinedFollower,
	Fading,
};

enum class G_PieceRole : uint8_t
{
	Seeker,
	Wanderer,
	Gregarious,
	Count
};

struct G_Piece
{
	float ColourIndex;

	SDL_Point SequencePositions[SEQUENCE_COUNT_MAX];
	uint8_t SequenceIndex = 0;
	uint8_t SequenceCount = 0;
	
	G_PieceRole Role = G_PieceRole::Gregarious;
	G_PieceRank Rank = G_PieceRank::Singleton;
	int8_t CombinedPieceIndex = INVALID_PIECE;
	int8_t MatchPieceIndex = INVALID_PIECE;
	int8_t LastCombinedPieceIndex = INVALID_PIECE;

	int8_t X;
	int8_t Y;
	int8_t PrevX;
	int8_t PrevY;
	int8_t StartX;
	int8_t StartY;

	static bool CanCombine(const G_Piece& piece_a, const G_Piece& piece_b);
};

struct G_Board
{
	G_Piece Pieces[PIECE_COUNT];
	int8_t PieceGridMap[GRID_WIDTH * GRID_HEIGHT];
	int8_t PieceCount = 0;

	G_Board();
	void Validate() const;
	void RandomizeRoles();
	void ResetAllPieces();
	void RemoveFadingPieces();
	void RemoveAllBut(int8_t piece_index);
	void RemovePiece(int8_t piece_index);
	int8_t PieceAt(int x, int y) const;
	int8_t NewPiece(float colour_index);
	void PutPiece(int8_t piece_index, int x, int y);
	void ResetPiece(uint8_t piece_index);
	void MovePiece(int8_t piece_index, int new_x, int new_y);
	void CombinePieces(int8_t piece_a_index, int8_t piece_b_index, int new_x, int new_y);
	void SplitPieces(int8_t piece_a_index, int new_a_x, int new_a_y);

	int GenerateRandomAvailablePositionsAround(
		SDL_Point* output_points,
		int requested_count,
		int center_x,
		int center_y,
		int8_t ignore_piece_index) const;

	SDL_Point NearestOpenPieceTo(int x, int y, int8_t ignore_piece_index) const;
	
	static size_t GridIndex(int x, int y);
};